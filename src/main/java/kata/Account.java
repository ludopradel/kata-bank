package kata;

import kata.exceptions.NotEnoughMoneyException;
import kata.exceptions.TransferNotAllowedException;

public class Account {

    private final Customer owner;
    private double balance;
    private final AccountType type;
    private boolean isClosed = false;

    public Account(AccountType accountType, Customer customer) {
        this.type = accountType;
        this.balance = 0.0;
        this.owner = customer;
    }

    public void deposit(double amount) {
        if (!isClosed)
            balance += amount;
    }

    public void transferMoneyTo(Account otherAccount, double amount) throws NotEnoughMoneyException, TransferNotAllowedException {
        if (this.balance <= amount) {
            throw new NotEnoughMoneyException();
        }

        if (!owner.equals(otherAccount.owner)) {
            throw new TransferNotAllowedException("Account's owners are different");
        }

        if (otherAccount.isClosed) {
            throw new TransferNotAllowedException("Destination account is closed");
        }

        if (!isClosed) {
            balance -= amount;
            otherAccount.balance += amount;
        }
    }

    public double balance() {
        return balance;
    }

    public void close() {
        isClosed = true;
    }
}
